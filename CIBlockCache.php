<?php 
class CIBlockCache
{
	public function __construct()
	{
		/** Подключаем модуль **/
		\Bitrix\Main\Loader::includeModule("iblock");
	}
	
	/** Кешированный метод получения списка элементов инфоблока **/
	public function getElementList($arFilter = array(), $arSort = array("ID" => "ASC"), $arField = array("ID", "NAME"))
	{
		/** Список полученных элементов **/
		$arEl = array();		
		/** Время жизни кеша **/
		$cache_time = "86400";
		/** Уникальный ключ кеширования **/
		$cache_id = md5(json_encode($arSort).json_encode($arFilter).json_encode($arField));
		
		$oCache = \Bitrix\Main\Data\Cache::createInstance();		
		if ($oCache->initCache($cache_time, $cache_id, "iblockcache")) {
			/** Если уникальный ключ есть в кеше, то берем значения из кеша **/
			$arEl = $oCache->GetVars();
		} else if ($oCache->startDataCache()) {
			/** Если уникальный ключ отсуствует к кеше, то получаем данные при помощи API **/
			$oRes = CIBlockElement::GetList($arSort, $arFilter, false, false, $arField);
			while ($arRes = $oRes->Fetch()) {
				$arEl[] = $arRes;
			}
			/** Сохраняем данные к кеше **/
			$oCache->endDataCache($arEl);
		}
				
		return $arEl;
	}
}