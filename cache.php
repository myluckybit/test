<?php 
/** Предположим, что скрипт запускается из консоли, и например лежит на первом уровне вложенности, от корня установки Битрикс **/
$arPath = pathinfo(__FILE__);
/** Определяем DOCUMENT_ROOT **/
$_SERVER["DOCUMENT_ROOT"] = realpath($arPath["dirname"] . "/..");
/** Подключаем пролог **/
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
/** Подключаем класс лежащий в этой же папке **/
require("CIBlockCache.php");

/** Создаем объект класса **/
$oIBlockCache = new CIBlockCache();
/** Получаем список элементов инфоблока **/
$arEl = $oIBlockCache->getElementList(array("IBLOCK_ID" => 26), array("ID" => "ASC"), array("ID", "NAME"));
print_r($arEl);